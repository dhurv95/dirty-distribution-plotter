#include<cstdio>
#include<cmath>
#include<cstdlib>

int main(int argc, char* argv[])
{
    //argv[1]: Datafile Name
    //argv[2]: Output plot file name
    //argv[3]: bin width
    //argv[4]: plot y label
    //argv[5]: plot x label

    double binw=atof(argv[3]);

    FILE *fp = fopen("tmp_plot.plt","w");

    fprintf(fp,"set term png\n");
    fprintf(fp,"set output \"%s\"\n",argv[2]);
    fprintf(fp,"set key off\nset border 3\n");
    fprintf(fp,"set boxwidth 0.05 absolute\nset style fill solid 1.0 noborder\n");
    fprintf(fp,"bin_width=%lf\n",binw);
    fprintf(fp,"set xlabel \"%s\"\n",argv[5]);
    fprintf(fp,"set ylabel \"%s\"\n",argv[4]);
    fprintf(fp,"bin_number(x) = floor(x/bin_width)\n");
    fprintf(fp,"rounded(x) = bin_width * ( bin_number(x) + 0.5 )\n");
    fprintf(fp,"plot \"%s\" using (rounded($1)):(1) smooth frequency with boxes",argv[1]);   
    fclose(fp);

    system("gnuplot tmp_plot.plt");
    system("rm temp_plot.plt");
    
    return 0; 
}